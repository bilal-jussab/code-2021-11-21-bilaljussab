import unittest

from main import calculate_height_m2, calculate_bmi_category_risk,\
    count_category, calculate_bmi

persons = [
    {'Gender': 'Male', 'HeightCm': 160, 'WeightKg': 80},
    {'Gender': 'Male', 'HeightCm': 170, 'WeightKg': 60},
    {'Gender': 'Male', 'HeightCm': 180, 'WeightKg': 120},
    {'Gender': 'Female', 'HeightCm': 165, 'WeightKg': 62},
    {'Gender': 'Female', 'HeightCm': 155, 'WeightKg': 70},
    {'Gender': 'Female', 'HeightCm': 145, 'WeightKg': 82}
]

mock_persons_response = [
    {'Gender': 'Male', 'HeightCm': 160, 'WeightKg': 80, 'BMI': 31.25,
     'BMI Category': 'Moderately obese', 'Health Risk': 'Medium risk'},
    {'Gender': 'Male', 'HeightCm': 170, 'WeightKg': 60, 'BMI': 20.76,
     'BMI Category': 'Normal weight', 'Health Risk': 'Low risk'},
    {'Gender': 'Male', 'HeightCm': 180, 'WeightKg': 120, 'BMI': 37.04,
     'BMI Category': 'Severely obese', 'Health Risk': 'High risk'},
    {'Gender': 'Female', 'HeightCm': 165, 'WeightKg': 62, 'BMI': 22.79,
     'BMI Category': 'Normal weight', 'Health Risk': 'Low risk'},
    {'Gender': 'Female', 'HeightCm': 155, 'WeightKg': 70, 'BMI': 29.17,
     'BMI Category': 'Overweight', 'Health Risk': 'Enhanced risk'},
    {'Gender': 'Female', 'HeightCm': 145, 'WeightKg': 82, 'BMI': 39.05,
     'BMI Category': 'Severely obese', 'Health Risk': 'High risk'}
]

class TestCase(unittest.TestCase):

    def test_calculate_height_m2_case_1(self):
        height_cm = 200
        height_m2 = calculate_height_m2(height_cm)
        self.assertEqual(height_m2, 4.0)

    def test_calculate_height_m2_case_2(self):
        height_cm = 171
        height_m2 = calculate_height_m2(height_cm)
        self.assertEqual(height_m2, 2.92)

    def test_calculate_bmi_category_risk_1(self):
        bmi = 18
        cat, risk = calculate_bmi_category_risk(bmi)
        self.assertEqual(cat, "Underweight")
        self.assertEqual(risk, "Malnutrition risk")

    def test_calculate_bmi_category_risk_2(self):
        bmi = 22
        cat, risk = calculate_bmi_category_risk(bmi)
        self.assertEqual(cat, "Normal weight")
        self.assertEqual(risk, "Low risk")

    def test_calculate_bmi_category_risk_4(self):
        bmi = 26
        cat, risk = calculate_bmi_category_risk(bmi)
        self.assertEqual(cat, "Overweight")
        self.assertEqual(risk, "Enhanced risk")

    def test_calculate_bmi_category_risk_4(self):
        bmi = 32
        cat, risk = calculate_bmi_category_risk(bmi)
        self.assertEqual(cat, "Moderately obese")
        self.assertEqual(risk, "Medium risk")

    def test_calculate_bmi_category_risk_5(self):
        bmi = 36
        cat, risk = calculate_bmi_category_risk(bmi)
        self.assertEqual(cat, "Severely obese")
        self.assertEqual(risk, "High risk")

    def test_calculate_bmi_category_risk_6(self):
        bmi = 42
        cat, risk = calculate_bmi_category_risk(bmi)
        self.assertEqual(cat, "Very severely obese")
        self.assertEqual(risk, "Very high risk")


    def test_count_category_normal(self):
        count = count_category(mock_persons_response, "Normal weight")
        self.assertEqual(count, 2)

    def test_count_category_mod_obese(self):
        count = count_category(mock_persons_response, "Moderately obese")
        self.assertEqual(count, 1)

    def test_calculate_bmi(self):
        new_list = calculate_bmi(persons)
        self.assertEqual(new_list, mock_persons_response)


if __name__ == '__main__':
    unittest.main()