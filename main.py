"""
This script calculates the BMI of people using weight and height.
It provides back data to the original list, adding BMI, Category and Risk

Author: Bilal Jussab
Date: 21-11-2021
"""
def calculate_height_m2(height_cm):
    """
    This function converts cm to m
    then squares it by doing m*m
    :param height_cm:
    :return: height_m2 to 2 decimal places
    """
    height_m = height_cm / 100
    height_m2 = height_m * height_m
    return float("{:.2f}".format(height_m2))


def calculate_bmi(persons_list):
    """
    This function calculates the BMI using the formula;
    "BMI(kg/m2) = mass(kg) / height(m)2"
    :return:
    """
    for person in persons_list:
        height_m2 = calculate_height_m2(person.get("HeightCm"))
        bmi = person.get("WeightKg") / height_m2
        category, risk = calculate_bmi_category_risk(bmi)
        person["BMI"] = float("{:.2f}".format(bmi))
        person["BMI Category"] = category
        person["Health Risk"] = risk
    return persons_list


def calculate_bmi_category_risk(bmi):
    """
    This function calculates the BMI category and health risk using the BMI.
    :param bmi:
    :return:
    """
    if bmi < 18.4:
        category = "Underweight"
        risk = "Malnutrition risk"
    elif 18.5 < bmi < 24.9:
        category = "Normal weight"
        risk = "Low risk"
    elif 25 < bmi < 29.9:
        category = "Overweight"
        risk = "Enhanced risk"
    elif 30 < bmi < 34.9:
        category = "Moderately obese"
        risk = "Medium risk"
    elif 35 < bmi < 39.9:
        category = "Severely obese"
        risk = "High risk"
    else:
        category = "Very severely obese"
        risk = "Very high risk"
    return category, risk


def count_category(persons_list, category):
    """
    Simple function to count the number of people who are considered
    "Overweight" according to BMI ranges.
    :param category: The category to count
    :return: counter
    """
    counter = 0
    for person in persons_list:
        if person.get("BMI Category") == category:
            counter += 1
    return counter


if __name__ == "__main__":
    persons = [
        {"Gender": "Male", "HeightCm": 171, "WeightKg": 96},
        {"Gender": "Male", "HeightCm": 161, "WeightKg": 85},
        {"Gender": "Male", "HeightCm": 180, "WeightKg": 77},
        {"Gender": "Female", "HeightCm": 166, "WeightKg": 62},
        {"Gender": "Female", "HeightCm": 150, "WeightKg": 70},
        {"Gender": "Female", "HeightCm": 167, "WeightKg": 82}
    ]

    persons = calculate_bmi(persons)
    print(persons)
    cat_count = count_category(persons, "overweight")
    print("Count: ", cat_count)
